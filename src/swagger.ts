import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export async function useSwagger(app: INestApplication): Promise<void> {
  const config = new DocumentBuilder()
    .setTitle('Application')
    .addBasicAuth()
    .addBearerAuth()
    .setDescription(
      'Welcome! Using endpoints below to interact with microservice',
    )
    .setContact(
      'Ledius Group Holding',
      'https://ledius.ru',
      'outcome@ledius.ru',
    )
    .setLicense('MIT', 'https://choosealicense.com/licenses/mit')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('/api/docs', app, document, {
    customSiteTitle: 'API',
    customCssUrl: '/public/swagger/theme-feeling-blue.css',
  });
}
