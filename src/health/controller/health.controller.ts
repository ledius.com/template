import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@Controller()
@ApiTags('Health')
export class HealthController {
  @Get('health')
  @ApiOkResponse({
    isArray: true,
  })
  public async getServices(): Promise<[]> {
    return [];
  }
}
