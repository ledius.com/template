import { Module } from '@nestjs/common';
import { HealthController } from './controller/health.controller';
import { LoggerModule } from '@ledius/logger';

@Module({
  imports: [LoggerModule],
  controllers: [HealthController],
})
export class HealthModule {}
