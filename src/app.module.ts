import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { DatabaseModule } from '@app/database/database.module';
import { APP_FILTER, APP_INTERCEPTOR, RouterModule } from '@nestjs/core';
import { RequestContextModule } from '@ledius/request-context';
import { LoggerModule } from '@ledius/logger';
import { routes } from '@app/routes';
import { ServeStaticModule } from '@nestjs/serve-static';
import path from 'path';
import { FeatureModule } from '@ledius/feature/dist/feature.module';
import { BaseExceptionFilter } from '@app/common/filters/base-exception.filter';

@Module({
  imports: [
    DatabaseModule,
    RequestContextModule,
    LoggerModule,
    RouterModule.register(routes),
    ServeStaticModule.forRoot({
      rootPath: path.join(__dirname, '..', 'public'),
      serveRoot: '/public',
    }),
    FeatureModule.forRoot(),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: BaseExceptionFilter,
    },
  ],
})
export class AppModule {}
