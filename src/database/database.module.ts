import { Module } from '@nestjs/common';
import { LocalEnvService } from '@app/local-env/services/local-env.service';
import { LocalEnvPathEnum } from '@app/local-env/contants/local-env-path.enum';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { LocalEnvModule } from '@app/local-env/local-env.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [LocalEnvModule],
      useFactory: async (env: LocalEnvService) => ({
        type: 'postgres',
        host: env.getSafety(LocalEnvPathEnum.DB_HOST),
        port: +env.getSafety(LocalEnvPathEnum.DB_PORT),
        username: env.getSafety(LocalEnvPathEnum.DB_USER),
        password: env.getSafety(LocalEnvPathEnum.DB_PASS),
        database: env.getSafety(LocalEnvPathEnum.DB_NAME),
        namingStrategy: new SnakeNamingStrategy(),
      }),
      inject: [LocalEnvService],
    }),
  ],
})
export class DatabaseModule {}
